import { Component } from '@angular/core';

import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Admin } from '../class/admin';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css'],
})
export class AdminComponent {
  admin: Admin = new Admin();
  constructor(private router: Router, private httpClient: HttpClient) {}
  userForm: FormGroup = new FormGroup({
    UserName: new FormControl('', [Validators.required]),
    Password: new FormControl('', [Validators.required]),
  });
  adminlogin() {
    this.httpClient
      .post('http://localhost:8090/admin/login', this.admin)
      .subscribe(
        (response) => {
          console.log('successfully logged');
          this.chart();
        },
        (error) => {
          alert('password is incorrect');
        }
      );
  }
  chart() {
    this.router.navigateByUrl('/chart');
  }
}
