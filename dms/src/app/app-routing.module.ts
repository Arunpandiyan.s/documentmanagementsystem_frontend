import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UploadComponent } from './upload/upload.component';
import { ChartComponent } from './chart/chart.component';
import { AdminComponent } from './admin/admin.component';
import { UserComponent } from './user/user.component';
import { SignupComponent } from './signup/signup.component';
import { UserpageComponent } from './userpage/userpage.component';
import { ViewfilesComponent } from './viewfiles/viewfiles.component';

const routes: Routes = [
  { path: 'upload', component: UploadComponent },
  { path: 'chart', component: ChartComponent },
  { path: '', component: AdminComponent },
  { path: 'userpage', component: UserpageComponent },
  { path: 'user', component: UserComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'viewfiles', component: ViewfilesComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
