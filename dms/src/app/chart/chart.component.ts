import { Component } from '@angular/core';
import { Chart } from 'chart.js';
import { DmsserviceService } from '../service/dmsservice.service';
import { Files } from '../class/files';
import { Signupclass } from '../signupclass';
@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css'],
})
export class ChartComponent {
  public chart: any;
  file: Files[] = [];

  user: Signupclass[] = [];
  createdby = 'arunpandiyan';

  constructor(public dms: DmsserviceService) {}

  ngOnInit(): void {
    this.dms.getUsers().subscribe((Response) => {
      this.user = Response;
      // this.assignUser();
    });
  }
  assignUser() {
    // let users = [];
    // for (let i = 0; i < this.user.length; i++) {
    //   users.push(this.user[i].username);
    // }
    // console.log(users);
  }

  getFileForUser(uname: any) {
    let name = uname;
    this.dms.getFilesForUser(name).subscribe((Response) => {
      this.file = Response;
      this.createChart();
    });
  }

  createChart() {
    let labelName = [];
    let dataSize = [];
    for (let i = 0; i < this.file.length; i++) {
      labelName.push(this.file[i].filename);
      dataSize.push(this.file[i].fileSize);
    }
    this.chart = new Chart('MyChart', {
      type: 'line',
      data: {
        labels: labelName,
        datasets: [
          {
            data: dataSize,
            borderWidth: 1,
            fill: false,
          },
        ],
      },
      options: {
        aspectRatio: 5.5,
      },
    });
  }
}
