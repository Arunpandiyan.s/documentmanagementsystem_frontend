import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Signupclass } from '../signupclass';
import { Files } from '../class/files';

@Injectable({
  providedIn: 'root',
})
export class DmsserviceService {
  constructor(private httpClient: HttpClient) {}

  createuser(signupclass: Signupclass): Observable<Signupclass[]> {
    return this.httpClient.post<Signupclass[]>(
      'http://localhost:8090/createuser',
      signupclass
    );
  }

  getUsers() {
    return this.httpClient.get<Signupclass[]>('http://localhost:8090/user/all');
  }

  getFiles() {
    return this.httpClient.get<Files[]>(
      'http://localhost:8090/file/view/files'
    );
  }
  getFilesForUser(name: any) {
    let createdby = name;
    console.log(createdby);
    return this.httpClient.get<Files[]>(
      'http://localhost:8090/file/view/files/' + createdby
    );
  }
  download(filename: string) {
    return this.httpClient.get(
      'http://localhost:8090/file/download/' + filename,
      { responseType: 'blob' as 'json' }
    );
    console.log(filename);
  }
}
