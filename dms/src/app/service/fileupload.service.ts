import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Signupclass } from '../signupclass';
import { Files } from '../class/files';

@Injectable({
  providedIn: 'root',
})
export class FileuploadService {
  user: Signupclass = new Signupclass();

  constructor(private httpClient: HttpClient) {}

  save(user: Signupclass) {
    this.user = user;
    console.log('save method');
  }

  upload(file: any): Observable<any> {
    let username = this.user.username;
    let email = this.user.email;
    console.log(username);
    console.log(email);
    const formData = new FormData();
    formData.append('file', file, file.name);
    return this.httpClient.post(
      'http://localhost:8090/file/upload/' + username + '/' + email,
      formData,
      {
        responseType: 'text',
      }
    );
  }
}
