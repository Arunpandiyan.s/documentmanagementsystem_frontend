import { Component } from '@angular/core';
import { Signupclass } from '../signupclass';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DmsserviceService } from '../service/dmsservice.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
})
export class SignupComponent {
  signupclassref: Signupclass = new Signupclass();

  constructor(private router: Router, private dmsservice: DmsserviceService) {}
  userForm: FormGroup = new FormGroup({
    UserName: new FormControl('', [Validators.required]),
    Password: new FormControl('', [
      Validators.required,
      Validators.minLength(8),
    ]),
    Email: new FormControl('', [
      Validators.required,
      Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'),
    ]),
    Role: new FormControl('', [Validators.required]),
    Experience: new FormControl('', [Validators.required, Validators.min(2)]),
    dob: new FormControl('', [Validators.required]),
  });
  SubmitData() {
    this.dmsservice.createuser(this.signupclassref).subscribe((data) => {
      console.log('success');
    });
    this.touser();
  }
  touser() {
    this.router.navigateByUrl('/user');
  }
}
