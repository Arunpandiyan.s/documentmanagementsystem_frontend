import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { FileuploadService } from '../service/fileupload.service';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css'],
})
export class UploadComponent {
  shortLink: string = '';
  loading: boolean = false;
  file: any;
  success: any;
  constructor(
    public dialog: MatDialog,
    public fileuploadService: FileuploadService
  ) {}
  form: FormGroup = new FormGroup({
    Title: new FormControl('', [Validators.required]),
    file: new FormControl('', [Validators.required]),
  });

  onChange(event: any) {
    this.file = event.target.files[0];
  }

  onUpload() {
    this.loading = !this.loading;
    console.log(this.file);
    this.fileuploadService.upload(this.file).subscribe((response: any) => {
      console.log(response);
      this.closeDialog();
    });
  }
  closeDialog() {
    this.dialog.closeAll();
    alert('File uploaded successfully');
  }
}
