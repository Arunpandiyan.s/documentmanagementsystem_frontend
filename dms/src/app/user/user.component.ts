import { Component } from '@angular/core';
import { Signupclass } from '../signupclass';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { FileuploadService } from '../service/fileupload.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
})
export class UserComponent {
  user: any;
  signup: Signupclass = new Signupclass();
  constructor(
    private router: Router,
    private http: HttpClient,
    public fileuploadservice: FileuploadService
  ) {}
  userForm: FormGroup = new FormGroup({
    UserName: new FormControl('', [Validators.required]),
    Password: new FormControl('', [Validators.required]),
  });
  login() {
    this.http.post('http://localhost:8090/user/login', this.signup).subscribe(
      (response) => {
        this.user = response;
        console.log(this.user);
        this.SendUserToService();
        this.userpage();
      },
      (error) => {
        alert('UserNotFound');
      }
    );
  }
  userpage() {
    this.router.navigateByUrl('/userpage');
  }
  SendUserToService() {
    this.fileuploadservice.save(this.user);
  }
}
