import { Component } from '@angular/core';
import { UploadComponent } from '../upload/upload.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-userpage',
  templateUrl: './userpage.component.html',
  styleUrls: ['./userpage.component.css'],
})
export class UserpageComponent {
  constructor(private dialog: MatDialog) {}
  upload() {
    this.dialog.open(UploadComponent, { width: '80%', height: '80%' });
  }
}
