import { Component, OnInit } from '@angular/core';
import { Files } from '../class/files';
import { DmsserviceService } from '../service/dmsservice.service';

@Component({
  selector: 'app-viewfiles',
  templateUrl: './viewfiles.component.html',
  styleUrls: ['./viewfiles.component.css'],
})
export class ViewfilesComponent implements OnInit {
  file: Files[] = [];
  table: Files[] = [];

  constructor(public dms: DmsserviceService) {}
  ngOnInit(): void {
    this.dms.getFiles().subscribe((Response) => {
      this.file = Response;
    });
  }

  clicked(filename: string) {
    console.log('clicked', filename);
    this.dms.download(filename).subscribe((Response) => {
      console.log('download finished');
    });
  }
}
